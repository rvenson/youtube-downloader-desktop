# Youtube Music Downloader Desktop

## Instalation

* Install NPM
* Execute ``npm install`` in project folder
* Download ffmpeg library and put ``ffmpeg.exe`` at ``/lib/ffmpeg/bin/``
* Create ``download`` folder at root directory

## Test

* Execute ``npm start`` at root folder

## Build release

* Execute ``npm run pack``
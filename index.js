const { app, Menu, Tray, clipboard, shell, dialog } = require('electron')
const youtube = require('youtube-mp3-downloader')
const { resolve } = require('path')

var tray = null

app.on('ready', () => {
    tray = new Tray(resolve(__dirname, 'assets', 'tray-icon.png'))

    const contextMenu = Menu.buildFromTemplate([
        { label: 'Download from clipboard', type : 'normal', click: () => {
            console.log(clipboard.readText())

            const linkId = clipboard.readText()

            //Configure YoutubeMp3Downloader with your settings
            var YD = new youtube({
                "ffmpegPath": "lib/ffmpeg/bin/ffmpeg.exe",        // Where is the FFmpeg binary located?
                "outputPath": "download",    // Where should the downloaded and encoded files be stored?
                "youtubeVideoQuality": "highest",       // What video quality should be used?
                "queueParallelism": 2,                  // How many parallel downloads/encodes should be started?
                "progressTimeout": 2000                 // How long should be the interval of the progress reports
            })

            //Download video and save as MP3 file
            YD.download(linkId, linkId + ".mp3");

            YD.on("finished", function (err, data) {
                console.log(JSON.stringify(data));
                dialog.showMessageBox({ message : JSON.stringify(data), type : 'info', title : 'Download complete'}, () => {
                    shell.showItemInFolder(resolve('download', linkId + ".mp3"))
                })
            })

            YD.on("error", function (error) {
                console.log(error);
                dialog.showMessageBox({ message : error, type : 'error', title: 'Error'})
            })

            YD.on("progress", function (progress) {
                console.log(JSON.stringify(progress));
            })
        }},
        { label: 'Quit', type : 'normal', click: () => {
            app.quit()
    }},
    ])

    tray.setToolTip('Youtube Music Downloader')
    tray.setContextMenu(contextMenu)
})